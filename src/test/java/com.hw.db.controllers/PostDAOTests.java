package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static com.hw.db.DAO.PostDAO.POST_MAPPER;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PostDAOTests {
  private Post firstPost;
  java.sql.Timestamp timestamp = java.sql.Timestamp.valueOf("2007-09-23 10:10:10.0");
  java.sql.Timestamp timestamp2 = java.sql.Timestamp.valueOf("2009-09-23 10:10:10.0");

  @BeforeEach
  @DisplayName("forum creation test")
  void createForumTest() {
    firstPost = new Post(
      "author",
      timestamp,
      "forum",
      "message",
      1,
      1,
      false
    );
  }

  @Test
  @DisplayName("author + message + created")
  void PostDAOTest1() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    Post post = new Post(
      "author1",
      timestamp2,
      "forum1",
      "message1",
      2,
      2,
      false
    );
    Mockito.when(mockJdbc.queryForObject("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;", POST_MAPPER, 1))
           .thenReturn(firstPost);

    PostDAO postDao = new PostDAO(mockJdbc);
    PostDAO.setPost(1, post);
    verify(mockJdbc).update(
      Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
      Mockito.eq(new Object[]{"author1", "message1", timestamp2, 1}),
      Mockito.any(PostDAO.PostMapper.class)
    );
  }

  @Test
  @DisplayName("null author + message + null created")
  void PostDAOTest2() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    Post post = new Post(
      null,
      null,
      "forum1",
      "message1",
      2,
      2,
      false
    );
    Mockito.when(mockJdbc.queryForObject("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;", POST_MAPPER, 1))
           .thenReturn(firstPost);

    PostDAO postDao = new PostDAO(mockJdbc);
    PostDAO.setPost(1, post);
    verify(mockJdbc).update(
      Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
      Mockito.eq(new Object[]{"message1", 1}),
      Mockito.any(PostDAO.PostMapper.class)
    );
  }

  @Test
  @DisplayName("null author + null message + created")
  void PostDAOTest3() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    Post post = new Post(
      null,
      timestamp2,
      "forum1",
      null,
      2,
      2,
      false
    );
    Mockito.when(mockJdbc.queryForObject("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;", POST_MAPPER, 1))
           .thenReturn(firstPost);

    PostDAO postDao = new PostDAO(mockJdbc);
    PostDAO.setPost(1, post);
    verify(mockJdbc).update(
      Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
      Mockito.eq(new Object[]{timestamp2, 1}),
      Mockito.any(PostDAO.PostMapper.class)
    );
  }

  @Test
  @DisplayName("nothing")
  void PostDAOTest4() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    Post post = new Post(
      null,
      null,
      "forum1",
      null,
      2,
      2,
      false
    );
    Mockito.when(mockJdbc.queryForObject("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;", POST_MAPPER, 1))
           .thenReturn(firstPost);

    PostDAO postDao = new PostDAO(mockJdbc);
    PostDAO.setPost(1, post);
    Mockito.atMost(0);
  }
}
