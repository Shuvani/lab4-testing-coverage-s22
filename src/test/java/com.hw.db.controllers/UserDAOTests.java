package com.hw.db.controllers;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class UserDAOTests {
  @Test
  @DisplayName("email + fullname + about")
  void UserDAOTest1() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    UserDAO userDao = new UserDAO(mockJdbc);
    User user = new User("some","some@email.mu", "name", "nothing");
    UserDAO.Change(user);
    verify(mockJdbc).update(
      Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
      Mockito.eq(new String[]{"some@email.mu", "name", "nothing", "some"}),
      Mockito.any(UserDAO.UserMapper.class)
    );
  }

  @Test
  @DisplayName("null email + fullname + null about")
  void UserDAOTest2() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    UserDAO userDao = new UserDAO(mockJdbc);
    User user = new User("some",null, "name", null);
    UserDAO.Change(user);
    verify(mockJdbc).update(
      Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
      Mockito.eq(new String[]{"name", "some"}),
      Mockito.any(UserDAO.UserMapper.class)
    );
  }

  @Test
  @DisplayName("null email + null fullname + about")
  void UserDAOTest3() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    UserDAO userDao = new UserDAO(mockJdbc);
    User user = new User("some",null, null, "nothing");
    UserDAO.Change(user);
    verify(mockJdbc).update(
      Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
      Mockito.eq(new String[]{"nothing", "some"}),
      Mockito.any(UserDAO.UserMapper.class)
    );
  }

  @Test
  @DisplayName("nothing")
  void UserDAOTest4() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    UserDAO userDao = new UserDAO(mockJdbc);
    User user = new User("some",null, null, null);
    UserDAO.Change(user);
    Mockito.atMost(0);
  }
}
