package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Forum;
import com.hw.db.models.Message;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ForumDAOTests {
  private User loggedIn;
  private Forum toCreate;

  @BeforeEach
  @DisplayName("forum creation test")
  void createForumTest() {
    loggedIn = new User("some","some@email.mu", "name", "nothing");
    toCreate = new Forum(12, "some", 3, "title", "some");
  }

  @Test
  @DisplayName("User gets list of threads test #1")
  void ThreadListTest1() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forum = new ForumDAO(mockJdbc);
    ForumDAO.ThreadList("slug",null, null, null);
    verify(mockJdbc).query(
      Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"),
      Mockito.eq(new String[]{"slug"}), Mockito.any(ThreadDAO.ThreadMapper.class)
    );
  }

  @Test
  @DisplayName("User gets list of threads test #1")
  void ThreadListTest2() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forum = new ForumDAO(mockJdbc);
    ForumDAO.ThreadList("slug",null, "12.02.2019", null);
    verify(mockJdbc).query(
      Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created;"),
      Mockito.eq(new String[]{"slug", "12.02.2019"}), Mockito.any(ThreadDAO.ThreadMapper.class)
    );
  }

  @Test
  @DisplayName("User gets list of threads test #1")
  void ThreadListTest3() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forum = new ForumDAO(mockJdbc);
    ForumDAO.ThreadList("slug",null, "12.02.2019", true);
    verify(mockJdbc).query(
      Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created desc;"),
      Mockito.eq(new String[]{"slug", "12.02.2019"}), Mockito.any(ThreadDAO.ThreadMapper.class)
    );
  }

  @Test
  @DisplayName("User gets list of threads test #1")
  void ThreadListTest4() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forum = new ForumDAO(mockJdbc);
    ForumDAO.ThreadList("slug",10, "12.02.2019", true);
    verify(mockJdbc).query(
      Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created desc LIMIT ?;"),
      Mockito.eq(new Object[]{"slug", "12.02.2019", 10}), Mockito.any(ThreadDAO.ThreadMapper.class)
    );
  }

  @Test
  @DisplayName("User gets list of threads test #1")
  void ThreadListTest5() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forum = new ForumDAO(mockJdbc);
    ForumDAO.ThreadList("slug",10, "12.02.2019", null);
    verify(mockJdbc).query(
      Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created LIMIT ?;"),
      Mockito.eq(new Object[]{"slug", "12.02.2019", 10}), Mockito.any(ThreadDAO.ThreadMapper.class)
    );
  }

  @Test
  @DisplayName("User gets list of threads test #1")
  void ThreadListTest6() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forum = new ForumDAO(mockJdbc);
    ForumDAO.ThreadList("slug",10, null, true);
    verify(mockJdbc).query(
      Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created desc LIMIT ?;"),
      Mockito.eq(new Object[]{"slug", 10}), Mockito.any(ThreadDAO.ThreadMapper.class)
    );
  }

  @Test
  @DisplayName("since!=null, desc!=null && desc, limit!=null")
  void UserListTest1() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forum = new ForumDAO(mockJdbc);
    ForumDAO.UserList("slug",10, "12.02.2019", true);
    verify(mockJdbc).query(
      Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
      Mockito.eq(new Object[]{"slug", "12.02.2019", 10}), Mockito.any(UserDAO.UserMapper.class)
    );
  }

  @Test
  @DisplayName("since!=null, desc=null")
  void UserListTest2() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forum = new ForumDAO(mockJdbc);
    ForumDAO.UserList("slug",10, "12.02.2019", null);
    verify(mockJdbc).query(
      Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
      Mockito.eq(new Object[]{"slug", "12.02.2019", 10}), Mockito.any(UserDAO.UserMapper.class)
    );
  }

  @Test
  @DisplayName("since=null, desc=null, limit=null")
  void UserListTest3() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forum = new ForumDAO(mockJdbc);
    ForumDAO.UserList("slug",null, null, null);
    verify(mockJdbc).query(
      Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
      Mockito.eq(new String[]{"slug"}), Mockito.any(UserDAO.UserMapper.class)
    );
  }
}
