package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ThreadDAOTests {
  @Test
  @DisplayName("since, desc=true, limit")
  void treeSortTest1() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ThreadDAO threadDao = new ThreadDAO(mockJdbc);
    ThreadDAO.treeSort(1, 2, 3, true);
    verify(mockJdbc).query(
      Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
      Mockito.eq(new Integer[]{1, 3, 2}),
      Mockito.any(PostDAO.PostMapper.class)
    );
  }

  @Test
  @DisplayName("since, desc=null")
  void treeSortTest2() {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ThreadDAO threadDao = new ThreadDAO(mockJdbc);
    ThreadDAO.treeSort(1, 2, 3, null);
    verify(mockJdbc).query(
      Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
      Mockito.eq(new Integer[]{1, 3, 2}),
      Mockito.any(PostDAO.PostMapper.class)
    );
  }

}
